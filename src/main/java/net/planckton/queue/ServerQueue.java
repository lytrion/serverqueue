package net.planckton.queue;

import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.planckton.queue.queue.QueueManager;

public class ServerQueue extends Plugin {
	
	@Getter private static ServerQueue instance;
	
	@Getter private QueueManager queueManager;
	
    @Override
    public void onEnable() {
    	instance = this;
    	queueManager = new QueueManager();
    }

    @Override
    public void onDisable() {
    	instance = null;
    }
    
}
