package net.planckton.queue.queue;

import java.util.HashMap;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.planckton.queue.ServerQueue;

public class QueueManager implements Listener {
	
	@Getter private HashMap<String, Queue> queues;
	
	public QueueManager() {
		this.queues = new HashMap<String, Queue>();
		
		ProxyServer.getInstance().getPluginManager().registerListener(ServerQueue.getInstance(), this);
	}
	
	@EventHandler
	public void onServerDisconnect(PlayerDisconnectEvent event) {
		for (Queue queue : queues.values()) {
			queue.removePlayer(event.getPlayer().getName());
		}
	}
	
	@EventHandler
	public void onServerDisconnect(ServerDisconnectEvent event) {
		if (!event.getTarget().getName().equalsIgnoreCase("lobby")) return;
		
		for (Queue queue : queues.values()) {
			queue.removePlayer(event.getPlayer().getName());
		}
	}
}
