package net.planckton.queue.queue;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.planckton.queue.ServerQueue;
import net.planckton.queue.tasks.QueueTask;

public class Queue {
	
	@Getter private ServerInfo server;	
	@Getter private int serverLimit;
	@Getter private LinkedList<String> players;
	
	public Queue(ServerInfo server, int limit) {
		this.server = server;
		this.serverLimit = limit;
		this.players = new LinkedList<String>();	
		
		ServerQueue.getInstance().getProxy().getScheduler().schedule(ServerQueue.getInstance(), new QueueTask(this), 5, TimeUnit.SECONDS);
	}
	
	public int getPosition(String player) {
		return players.indexOf(player);
	}
	
	public void addPlayer(ProxiedPlayer player) {
		if (players.contains(player)) {
			player.sendMessage(new TextComponent("You are already in the queue!"));
			return;
		}
		
		players.add(player.getName());
	}
	
	public boolean removePlayer(String player) {
		return players.remove(player);
	}
	
	public void sendPositionUpdateMessage() {
		int i = 0;
		
		while (i <= players.size()) {
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(players.get(i));
			player.sendMessage(new TextComponent("Your position is now updated to #" + i));
			i++;
		}		
	}

}
