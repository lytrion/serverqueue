package net.planckton.queue.tasks;

import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.planckton.queue.queue.Queue;

@AllArgsConstructor
public class QueueTask implements Runnable {

	private Queue queue;
	
	public void run() {
		if (queue.getServer().getPlayers().size() < queue.getServerLimit()) {
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(queue.getPlayers().pop());
			if (player == null) return;
			
			player.connect(queue.getServer());
		}		
		
		queue.sendPositionUpdateMessage();
	}

}
